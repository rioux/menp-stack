const LocalStrategy = require('passport-local').Strategy;
const FacebookStrategy = require('passport-facebook').Strategy;
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;

const User = require('../models/users');
const { googleAppId, googleSecret, fbAppId, fbSecret, dev_url, prod_url } = require('./config');


console.log("Configuration de passport bb!")
var callbackURI = process.env.CALLBACK_URI;

module.exports = (passport) => {
  passport.serializeUser(async (user, done) => {
    return done(null, user);
  });
  
  passport.deserializeUser(async (user, done) => {
    try {
      //console.log("Log Passport User: ", user);
      console.log("deserializing");
      const u = await User.findById(user.id);
      done(null, u);
    } catch(err) {
      done(err, null);
    }
  });
  //LOCAL-LOGIN STRATEGY
  passport.use('local-login', new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password',
  }, async (username, password, done) => {
    try {
      //check if email exists
      const user = await User.findOne({username: username});
      if (!user) {
        return done(null, false, {message: "Unknown User!"});
      } 
      // Check if the passwords matches
        const pwdValidation = await user.validatePassword(password);
        console.log("PWD VALIDATION: ", pwdValidation);
        if (!pwdValidation) {
          return done(null, false, {message: "Unknown password!"});
        } else {
          console.log(`

        Voici le user : 
        ${user}

        `);
          return done(null, user);
        }
    } catch(err) {
      return done(err,false);
    }
  }));

  //LOCAL-REGISTER STRATEGY
  passport.use('local-register', new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password',
  }, async (username, password, done) => {
    try {
      const existingUser = await User.findOne({username: username});
      if (!existingUser) {
        const newUser = await new User({username: username});
        await newUser.setPassword(password);
        await newUser.save();
        console.log(`
        
        ${newUser}

        `);
        done(null, newUser);
      } else {
        const pwdValidation = await existingUser.validatePassword(password);
        if (!pwdValidation) {
          return done(null, false, {message: "Unknown password!"});
        } else {
          console.log(`
          
          Voici le existing user : 
          ${existingUser}

          `);
          done(null, existingUser);
        }
      }
    } catch(err) {
      return done(err,false);
    }
  }));

  //FACEBOOK STRATEGY

  passport.use('facebook-strategy', new FacebookStrategy({
    clientID: process.env.FB_APP_ID ||fbAppId,
    clientSecret: process.env.FB_SECRET || fbSecret,
    callbackURL: callbackURI + "/auth/facebook/callback",
    profileFields: ['id', 'emails', 'name', 'gender', 'profileUrl', 'displayName'],
    scope: ['email']
  },
  function(accessToken, refreshToken, profile, cb) {
    console.log("La fonction passport Facebook fonctionne.", profile);
    const userStatus = User.fbFindOrCreate(profile);
    console.log(`
        
        Voici le Fb user : 

        `, userStatus);
    return cb(null, userStatus);
  }
));

passport.use('google-strategy', new GoogleStrategy({
  clientID: process.env.GOOGLE_APP_ID || googleAppId,
  clientSecret: process.env.GOOGLE_SECRET || googleSecret,
  callbackURL: callbackURI + "/auth/google/callback"
},
function(accessToken, refreshToken, profile, cb) {
  console.log("La fonction passport Google fonctionne.");
  console.log(`This is the google profile:`, profile);
  const userStatus = User.googleFindOrCreate(profile);
  console.log(`
      
      Voici le Google user : 

      `, userStatus);
  return cb(null, userStatus);
}
));

}
