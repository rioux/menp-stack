const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const mongoose = require('mongoose');
const { sessionSecret, dbhost } = require('./config/config');
const bodyParser = require('body-parser');
const session = require('express-session');
const passport = require('passport');
require('dotenv').load();


var indexRouter = require('./routes/index');
var protectedRouter = require('./routes/profile');
var authRouter = require('./routes/auth');


var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(cookieParser());

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

//sessions 
app.use(session({
  secret: sessionSecret,
  resave: false,
  saveUninitialized: true,
  cookie: { secure: false }
}))
app.use(require('connect-flash')());
app.use(function (req, res, next) {
  res.locals.messages = require('express-messages')(req, res);
  next();
});


//Connecte le client mongoose à la bonne base de données
mongoose.connect(process.env.MLAB_URI , { useNewUrlParser: true });

//enregistre la base de donnédans une variable facilement accessible
var db = mongoose.connection;

app.use(passport.initialize());
app.use(passport.session());
require('./config/passport')(passport);
mongoose.Promise = global.Promise;


//Facilite le "error handling" des bases de donnnées
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
  // Signifie à la console que la connection s'est bien effectuée
  console.log("connected");
});

app.use('/', indexRouter);
app.use('/auth', authRouter);
app.use('/private', protectedRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
