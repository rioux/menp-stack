const express = require('express');
const router = express.Router();
const passport = require('passport');
require('../config/passport')(passport);

const { title } = require('../config/config.js');


/* GET home page. */
router.get('/signin', (req, res, next) => {
    return res.render('signin', {title: title})
})

router.get('/register', (req, res, next) => {
    return res.render('signup', {title: title})
})

router.post("/register", passport.authenticate('local-register', {
    failureRedirect : '/auth/register', // redirect back to the signup page if there is an error
    successRedirect : '/private', 
    failureFlash : true // allow flash messages
  }));

router.post('/logout', (req, res, next) => {
    req.session.destroy();
    return res.redirect('/');
});

router.get('/facebook',
  passport.authenticate('facebook-strategy'));

router.get('/facebook/callback',
  passport.authenticate('facebook-strategy', { 
    failureRedirect: '/auth/signin',
    successRedirect: '/private'
  }));


router.post('/signin', passport.authenticate('local-login', {
    failureRedirect : '/auth/signin', // redirect back to the signup page if there is an error
    successRedirect : '/private', 
    failureFlash : true // allow flash messages
  })
 );

 router.get('/google', passport.authenticate('google-strategy', { scope: ["profile", "email", "openid"], failureRedirect: "/" }));

 router.get('/google/callback', 
  passport.authenticate('google-strategy', { failureRedirect: '/login' }),
  function(req, res) {
    res.redirect('/private');
  });

module.exports = router;