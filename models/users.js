//Instancie les dépendances du modele
const mongoose = require('mongoose');
const crypto = require('crypto');
const { sessionSecret } = require('../config/config');

//Définit la structure des données pour le USER
var UserSchema = new mongoose.Schema({
    username: {
      type: String,
      unique: true,
      trim: true
    },
    salt: {
      type: String
    },
    hash: {
      type: String
    },
    facebook: {
      id : String,
      token : String,
      name : String,
      email : String
    },
    google: {
      id: String,
      token: String,
      name: String,
      email: String
    }
  });

  UserSchema.methods.setPassword = function(password) {
    this.salt = crypto.randomBytes(16).toString('hex');
    this.hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
    return this.hash, this.salt;
  };

  UserSchema.methods.validatePassword = function(password) {
    if (!this.salt) {
      return false;
    }
    const hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
    return this.hash === hash;
  };

  UserSchema.statics.fbFindOrCreate = async function(profile) {
    const existingUser = await User.findOne({"facebook.id": profile.id});
    await console.log(existingUser);
    if (existingUser) {
      console.log("Le user existe dans la BD");
      return existingUser;
    } else {
      const newUser = new User({
        username: profile.emails[0].value,
        facebook: {
          id: profile.id,
          name: profile.displayName
        }
      });
      const ser = await newUser.save();
      await console.log(ser);
      return newUser;
    }
  };

  UserSchema.statics.googleFindOrCreate = async function(profile) {
    const existingUser = await User.findOne({"google.id": profile.id});
    await console.log(existingUser);
    if (existingUser) {
      console.log("Le user existe dans la BD");
      return existingUser;
    } else {
      const newUser = new User({
        username: profile.emails[0].value,
        google: {
          id: profile.id,
          name: profile.displayName
        }
      });
      const ser = await newUser.save();
      await console.log(ser)
      return newUser;
    }
  };

  UserSchema.methods.generateJWT = function() {
    const today = new Date();
    const expirationDate = new Date(today);
    expirationDate.setDate(today.getDate() + 60);
  
    return jwt.sign({
      username: this.username,
      id: this._id,
      exp: parseInt(expirationDate.getTime() / 1000, 10),
    }, sessionSecret);
  }

  var User = mongoose.model('User', UserSchema);
  module.exports = User;
