# Welcome to my Express-Mongo-Pug Boilerplate
## Stop fooling around only to start a simple node.js project

I created this stack to facilite the integration of Express web server with a pug template engine and MongoDB Installation from Mlab.

Passport Integration comes out of the box, with all the views and routes needed. I implemented local strategy, Google and Facebook Strategy. 

You will need to configure your own developpers keys on both Facebook and Google platforms so you can replace the dummy data in config.sample.js (don't forget to whitelist the full callback URL on the developper platform)

It is possible to configure the settings in a config.samplejs file installed.
(This file exports a 'config' object. The settings are set and retrieved via dot notation)
___
## Configuring the boilerplate to work

___
### Installing and starting the stack

```
$ git clone https://github.com/jelecool/mongo-express-pug-node

$ npm install

$ npm start
```
### Protect your sensitive information

You need to take the file ./config/config.sample.js and edit it to enter all your sensitive information : Database connnexion information, project specific variables, api key and app secrets.

### Adding properties to the config.sample.js
*You should rename the file : config.js*

Create an object and then assign how may properties you need elsewhere. (port number, db connection, project name and branded copy.)

```
//Create the config object
var config = {};

// Set a property to the 'title' key of the object
config.title = 'My Application Title';
config.fbAppSecret = "123ghj13gh21312ih";

//Export the object so you can access it by requiring it elsewhere
module.exports = config;
```

### Getting options in a node.js file anywhere
Simply require the config object in a variable named how you like. (I named it 'config' to facilitate the comprehension of my code) 

```
// Require the config object and store it in a variable named 'config'
var config = require('./config/config.js')

[...]

// Render the template named index and assign the value of config.title to the title property of that template.
return res.render({title: config.title}, 'index')

OR Using ES6 Syntax
// Deconstruct the object while requiring it anywhere
const { fbAppSecret, title } = require('./config/config.js');

```

Thanks for using my boilerplate. And please, tell me some constructive comments! Safe coding guys!